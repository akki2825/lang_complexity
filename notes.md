## Measures of Linguistic complexity


Linguistic complexity has been used as a tool for describing and comparing languages [Dahl, 2004, Fenk, 2005]

There are two different ways of measuring linguistic complexity based on
 
1. <b>Traditional linguistic approach</b> - The traditional linguistic method of quantifying linguistic complexity is to count the number of constituents of the linguistic system in question (Bane, 2008). For example, word complexity and syllable complexity are defined as the average number of syllables per word and the average number of segments per syllable respectively in [Fenk, 2005].

2. <b>Information-theoretic approach</b> - The lanuage is defined as a system consisting of a finite set of linguistic units. It takes into account the predictability distribution estimated form a language model based on large corpora for quantifying linguistic complexity.


### Traditional linguistic approach


<b>Phonological complexity</b> : Phonological complexity can be measured by counting the size of syllable inventory and phonemic inventory [Bane, 2008]. 

Syllable complexity is calculated as the average number of segments (and tones, if applicable). The degree of syllable complexity is determined based on the maximally complex syllable structure (simple, moderately complex, and complex) [Dryer and Haspelmath, 2013].

<b>Morphological complexity</b> : Morphological complexity can be obtained by counting the number of inflectional categories which can be marked by verbs [Bane, 2008]. Inflection always complexifies grammar [McWhorter, 2001] while derivational morphology was considered more functional and thus, was excluded from the complexity metric.
	
<b>Syntactic complexity</b> : Syntactic complexity can be measured by the average number of specific syntactic constructions (e.g. nominals) per sentence and the average number of verbs per sentence [Chen & Zechner, 2011]. It is frequently used as a metric for the language proficiency of second language learners.

<b>Semantic complexity</b> : Semantic complexity can be computed by the average number of lemmas per homophones, taking the distribution of word frequencies into account. According to the result presented by Piantadosi and colleagues, high frequency and short words tend to encompass more meanings, thus, more ambiguity, which is related to the communicative efficiency [Bell et al., 2009]


### Information-theoretic approach

<b>Phonological complexity</b> : Phonological complexity can be obtained by the estimated average amount of information (in bits) contained per linguistic unit by means of the information measures such as Shannon entropy H(X) and conditional entropy H(X|C).
	The former quantifies the average amount of information from a unigram language
model without context while the latter computes the average amount of information taking contextual information into account.

<b>Morphological complexity</b> : Morphological complexity can be obtained by means of automatic unsupervised morphological analyzers such as Linguistica, Morfessor and UniMorph. 
The lexicon constructed by Linguistica consists o a set of stems, affixes and signatures. Signature here refers to a subset of affixes which can possibly combined with a subset of stems. The metric of morphological complexity (MC) was proposed in Bane, 2008 as follows, where DL (x) corresponds to the description length of x which is defined as the shortest description (that is, Kolmogorov complexity)  approximated by Linguistica and morphological complexity is computed as the ratio of the description length of inflectional morphology to the total information encoded by the lexicon.
	
The other measure of morphological complexity is computed using the average amount
of information per paradigm cell [Ackerman and Malouf, 2013].

<b>Syntactic complexity</b> : Syntactic complexity can be obtained by means of syntactic surprisal and lexicalized surprisal proposed by Demberg and Keller (Demberg and
Keller, 2008). Both measures can be computed using the equation provided below, using an elaborated language model such as probabilistic context-free grammar (PCFG),
which computes the probability of grammatical rules obtained from a syntactic tree. Syntactic surprisal quantifies the portion of the structural information between the words
Wk and Wk+1 ignoring the effect of word frequency while lexicalized surprisal employs
both the structural information and word frequency distributions.

<b>Semantic complexity</b> : Semantic aspect of communication are irrelevant to the engineering problem. The alternative method of counting the average number of lemmas
per homophones can be employed, considering the distribution of word frequency [Fenk-Oczlon, 2013].


Non-linguistic factors can be distinguished into two types: sociolinguistic and neurocognitive factors.

### Sociolinguistics factors

In sociolinguistics, Lupyan and Dale suggested that language structure is related to social environments such as speaker population size, geographical spread, and the degree of linguistic contact.
Regarding the relationship between phonological complexity and sociolinguistic factors, a positive correlation was found between speaker population size and phoneme in-
ventory size, using a sample of 250 languages by Hay and Bauer [Hay & Bauer, 2007]

### Neurocognitive factors

High frequency words require little memory effort and are often used in many different contexts. Thus, it requires more disambiguation effort from heares (Kello and Beltz, 2009)

“The entropy rate constancy principle” was proposed by Genzel and Charniak, which
asserts that speakers tend to maintain the constant rate of conditional entropy given the
previous elements during their utterances [Genzel & Charniak, 2002]. 

In the same lines, The uniform information density hypothesis was proposed by Levy and Jaeger. According to the UID hypothesis, speakers modulate the information density of their utterances in order to optimally transmit the information at a uniform rate, near the channel capacity. The UID hypothesis is focused on the wa how speakers plan and produce their utterances, based on the assumption that they do it efficiently due to several constraints imposed by speakers, hearers, and environments.
Thus, it is assumed that the efficient and optimal way of transmitting information is to maintain the information density of their utterances uniformly without exceeding the channel capacity

The UID hypothesis was attested by the results presented in several studies. To begin
with, Piantadosi and colleagues suggested that word length is better predicted by information density (obtained by using the previous word as contextual information) than by word frequency.

To READ: A cross-language perspective on speech information rate [Pellegrino, 2011]


### References:

[Dahl, 2004] Dahl, Ö. (2004). The Growth and Maintenance of Linguistic Complexity.

[Fenk-Oczlon & Fenk, 2005] Fenk-Oczlon, G. & Fenk, A. (2005). Crosslinguistic correlations between size of syllables, number of cases, and adposition order.

[Bane, 2008] Bane, M. (2008.) Quantifying and measuring morphological complexity.

[Dryer & Haspelmath, 2013] Dryer, M. S. & Haspelmath, M. (Eds.) (2013). The World Atlas of Language Structures Online.

[McWhorter, 2001] McWhorter, J. (2001) The world’s simplest grammars are creole grammars. Linguistic Typology.

[Chen & Zechner, 2011] Chen, M., & Zechner, K. (2011). Computing and evaluating syntactic complexity features for automated scoring of spontaneous non-native speech.

[Bell et al., 2009] Bell, A., Brenier, J. M., Gregory, M., Girand, C., & Jurafsky, D. (2009). Predictability effects on durations of content and function words in conversational English.

[Ackerman & Malouf, 2013] Ackerman, F. & Malouf, R. (2013). Morphological organization: The low conditional entropy conjecture.

[Fenk-Oczlon, 2013] Relationships between semantic complexity, structural complexity and markedness: Frequency matters.

[Hay & Bauer, 2007] Hay, J. & Bauer, L. (2007). Phoneme inventory size and population size.

[Genzel & Charniak, 2002] Genzel, D., & Charniak, E. (2002). Entropy rate constancy in text.

[Pellegrino, Coupé, & Marsico, 2011] Pellegrino, F., Coupé, C., & Marsico, E. (2011). A cross-language perspective on speech information rate.
