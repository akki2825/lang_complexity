# lang_complexity

Language complexity through complex networks is implemented in `complex_networks.ipynb`.

A theoretical approach to analysing Language complexity using Linguistic methods is given in `notes.md`.
